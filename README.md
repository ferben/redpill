This is Red Pill, a 3D OpenGL "Matrix" screensaver for macOS.<br/>
Copyright © 2002-2012 mathew <meta@pobox.com>.<br/>
Copyright © 2019-2023 František Erben <erben.fr@gmail.com>.

## Sources
Source by ferben [repository](https://bitbucket.org/ferben/redpill).

Binary (not signed/notarized) in [download section](https://bitbucket.org/ferben/redpill/downloads).

Looking for a old binary download by mathew? Check [the downloads section](https://github.com/lpar/RedPill/downloads).

## LICENSE
Red Pill is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or visit [http://www.fsf.org/](http://www.fsf.org/)

For more information about Red Pill and why you can't find it in the Mac App Store, see [http://meta.ath0.com/software/redpill/](http://meta.ath0.com/software/redpill/). Please note that Apple's terms and conditions on the App Store prohibit distribution of GPL-licensed software.

## Version history
##### Version 2.1.5 (February 2024)
Added thumbnails/icons. Japaneese localization removed.
##### Version 2.1.4 (October 2023)
Check compatibility with macOS 14 Sonoma. Minimal software requirement is macOS 10.13 and higher. No extra changes.
##### Version 2.1.3 (May 2021)
Build for running on Intel-based and ARM-based macs (Apple Silicon) natively. Minimal software requirement is macOS 10.9 and higher. No extra changes.
##### Version 2.1.1 (February 2019)
Forked and update to build on Xcode 10 and for macOS 10.10 and higher. Still depend on OpenGL which is deprecated by Apple and should be replaced by [Metal](https://developer.apple.com/documentation/metal) or another Apple API).